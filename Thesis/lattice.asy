bool less(string[] a, string[] b){
	return a.length< b.length;
}

string[][] power_set(string[] input){
	string[][] power_set={new string[]};
	for (string item: input){
		string[][] new_power_set;
		for(string[] subset: power_set){
			string[] new_subset;
			if(subset.length>0){
				new_power_set.push(subset);
				new_subset.append(copy(subset));
			} 
			new_subset.push(item);
			new_power_set.push(copy(new_subset));
		}
		power_set = new_power_set;
	}
	power_set = sort(power_set,less);
	return power_set;
}

string [][][] by_subset_size(string[][] set){
	int len = set.length;
	string[][][] new_set = new string[len][][];
	for(int i=len;i>0;i-=1){
		int cur_len = set[i-1].length;
		(new_set[cur_len-1]).push(set[i-1]);
	}
	for(int i=0;i<len;i+=1){
		if(new_set[i].length==0){
			new_set.delete(i);
			i-=1; len-=1;
		} else {
			int len2 = new_set[i].length;
			for(int j=0;j<len2;j+=1){
				if(new_set[i][j].length==0){
					new_set[i].delete(j);
					j=j-1; len2-=1;
				}
			}
		}
	}
	return new_set;
}

void print_set(string[][] power_set){
	int len = power_set.length;
	string out="";
	for(int i=len;i>0;i-=1){
		string[] subset = power_set[i-1];
		out+='{';
		for(int j=0; j<subset.length; j+=1){
			string item = subset[j];
			out+=item;
			if(j<subset.length -1 ){
				out+=", ";
			}
		}
		out+='}\n';
	}
	out +="{}";
	write(out);
}

void print_cat_set(string[][][] set){
	for(int j=0; j<set.length; j+=1){
		write("Len="+string(j));
		if(set[j].length>0){
			print_set(set[j]);
		}
	}
}
