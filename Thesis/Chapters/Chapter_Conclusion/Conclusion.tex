% !TeX spellcheck = fr_FR

Après un examen attentif de l'état de l'art sur les ressources lexicales, les niveaux et standards d'interopérabilité, ainsi que les techniques pour l'interopérabilité sémantique des ressources lexicales nous constatons les points suivants.

\begin{itemize}
	\item Les formats pour les données lexicales liées et les technologies du Web sémantique offrent une réponse globalement satisfaisante pour l'interopérabilité représentationnelle des \ac{RLS}.
	
	\item Les architectures d'alignement pour l'interopérabilité sémantique, ou du moins leurs implémentations pratiques ne sont pas suffisantes et présentent des défauts importants.
	\begin{itemize}
		\item Les architectures par transfert nécessitent de calculer et de stocker toutes les alignements par paires entre touts les ressources alignées. Se pose ainsi un problème de passage à l'échelle, et aussi une difficulté d'accès aux données dans un contexte trilingue, quadrilingue, etc. 
		\item Les architectures à base de pivot naturel utilisent les sens d'une langue comme pivot, ce qui biaise la ressource alignée vis-à-vis de la conceptualisation de cette langue en particulier. On introduit ainsi des phénomènes contrastifs artificiels, ou en d'autre termes, une perte de contraste entre les langues ayant des lexicalisations divergentes similaires.
		
		\item Les architectures à base de pivot artificiel, par exemple avec des acceptions interlingues, permettent de s'affranchir de certains des problèmes des deux autre types d'architecture. Cependant, un problème important qui se pose avec un véritable pivot interlingue est qu'il est très difficile à construire manuellement du fait de la difficulté à trouver des experts compétents. Il est de plus difficile de maintenir une communauté active pour la collaboration de contenu et de corrections. Il manque par ailleurs une vraie formalisation des acceptions interlingues qui permettrait une construction et une évaluation automatiques.
	\end{itemize}
	
	\item Il n'existe pratiquement aucune prise en compte de l'interopérabilité dynamique, surtout pour les \ac{RLS} provenant de ressource collaboratives évoluant sans cesse.
\end{itemize} 


\paragraph{} Partant de ces constats, notre premier travail a été d'attaquer le problème de la formalisation des acceptions interlingues. Sur cette idée, nous avons d'abord proposé un algorithme de construction initiale d'un alignement de ressources par acceptions interlingues. Puis, afin de permettre la compatibilité avec une prise en compte de l'interopérabilité dynamique, nous avons proposé des algorithmes pour les opérations de mise à jour pour les acceptions interlingues. 

\paragraph{} Dans un deuxième temps, nous avons examiné de manière plus pratique la mise en œuvre des algorithmes de construction d'acceptions, au travers d'un cas d'étude pratique, celui de DBNary. DBNary permet d'extraire les différentes éditions de langue de Wiktionary dans un format de données lexicales liées (\verb|lemon|), à ce titre c'est un candidat idéal. 

Cependant, l'algorithme de construction nécessite d'avoir préalablement établi tous les alignements de sens bilingues. Or les relation de traduction déjà présentes dans Wiktionary sont établies entre des entrées lexicales et des vocables, ce qui signifie qu'il faut d'abord les ramener au niveau des sens à la source comme à la cible. 

Nous avions tout d'abord proposé une approche pour rattacher les sources des relation de traduction au niveau du sens. Ensuite nous avons proposé de manière théorique des mesures de similarité translingue applicables à DBNary, ainsi qu'un algorithme pour aligner l'ensemble des relations de traduction aux sens, à la fois à la source et à la cible. 

\paragraph{} Dans un troisième temps, nous avons étudié les solutions possibles pour l'évaluation d'acceptions interlingues produites à partir de DBNary. En effectuant d'abord des évaluations in vitro. Nous avons constaté qu'il n'y a que très peu de données issues de Papillon et qu'il serait difficile de les exploiter de manière précise. Ensuite, nous avons proposé une évaluation in vivo au travers de la tâche de substitution lexicale issue de SemEval, ainsi qu'une possible adaptation des données de la tâche de désambiguïsation lexicale translingue. 

\paragraph{} Pour finir, nous avons présenté LexSemA une boite à outils logicielle générique pour les \ac{RLS}  allant de pair avec ce travail de thèse, qui regroupe et implémente tous les outils nécessaires pour la construction d'acceptions interlingues et pour leur évaluation.

\subsection{Limitations}
Malgré tout cela, la limitation principale des techniques de création d'acceptions interlingues reste très clairement que, pour la construction initiale, il faut établir toutes les paires d'alignements bilingues. 
Même si l'algorithme d'ajout permet de s'affranchir de la nécessité d'avoir toutes les paires d'alignements, il reste tout de même nécessaire d'aligner toutes les paires marquant une divergence de lexicalisation. 
Par ailleurs, étant donné que cet alignement se fera par \ac{AAS}, il y aura nécessairement des erreurs, qui viendront s'accumuler lors de la génération des acceptions. 
\subsubsection{Disponibilité de ressources pour la construction initiale}
Du fait de la première limitation, il faudra donc pouvoir réaliser l'alignement dans un nombre conséquent de paires de langues. Dans le cas de DBNary, étant donné que mise à part les 10-15 éditions les plus grandes il y a très peu d'entrées, il faudra inévitablement faire appel à d'autres ressources.

Il faut donc avoir des dictionnaires bilingues au niveau des mots pour la plupart des langues couverte par la ressource finale. Même si il n'existe pas de dictionnaires bilingues pour certaines paires, il est possible d'en générer à partir de corpus parallèles à la manière de \citet{Ammar2016}. Ainsi, le goulot d'étranglement en ce qui concerne les sources de connaissances sera le même que pour la traduction automatique, la disponibilité de ressources parallèles. 

La conséquence principale est qu'il sera très difficile d'intégrer des langues faiblement dotées dans la ressource finale sans au préalable doter la langue, comme c'est le cas dans la plupart des applications de \ac{TAL}. Même dans le cas de DBNary, cela signifie que beaucoup parmi les plus petites éditions pourront difficilement être intégrées. 

\subsubsection{Mise en pratique et évaluation réelle}
Les contributions de cette thèse sont pour la majeure partie théoriques et le \autoref{chap:alignemntacception} a illustré un nombre non négligeable d'obstacles à la construction. Le principal obstacle est l'établissement des alignements bilingues. Du fait de l'accumulation rapide des erreurs pour l'établissement des alignements bilingues préalables, il sera essentiel de travailler à la maximisation de la précision des alignements bilingues. 

\paragraph{} Les règles axiomatiques régissant les acceptions peuvent bien sûr être utilisées pour détecter certaines incohérences, comme décrit dans la partie du \autoref{chap:alignementinteroperabilite} décrivant les travaux de \citet{Teeraparbseree2005}. Cependant, cette détection est limitée à des cas très précis. Par ailleurs, ces erreurs pourront uniquement être détectées et non corrigées automatiquement. Il faudra donc nécessairement recourir à des correcteurs humains. Il est probable que l'initiative récente portant sur l'index interlingue collaboratif sera une solution viable pour apporter ce type de corrections, vu que les correction portent uniquement sur des alignements bilingues \citep{Bond2016}.

\paragraph{} Une chose qui manque est bien entendu la mise en pratique, même à petite échelle, malgré le fait que tous les outils pour le faire sont implémentés. Cette mise en pratique sera  l'une des premières choses à faire dans nos travaux futurs.

\subsection{Travail Futur}
Cette section porte sur le travail futur à réaliser suite à cette thèse. Les objectifs à court terme sont des projets de recherche courts de 6 à 12 mois et peuvent faire l'objet de sujets de stage de M2R. Les projets à moyen terme sont des projets nécessitant de 2 à 4 ans pour être menés à bien, et peuvent faire l'objet de sujets de thèse de doctorat. Les projets à long terme sont des projets nécessitant de 5 à 10 ans et correspondent à des objectifs de carrière dans l'optique de l'habilitation à diriger les recherches. 

\subsubsection{À court terme}

Il y a deux objectifs fondamentaux pouvant être réalisés à court terme:
\begin{itemize}
	\item Achever l'implémentation des outils logiciels:
	\begin{itemize}
		\item implémenter les algorithmes de création et de mise à jour;
		\item ajouter une représentation de travail de la ressource sans passer par des requêtes SPARQL afin d'optimiser la performance d'accès;
		\item optimiser et modulariser les dépendances afin d'améliorer la distribution des outils;
		\item assurer l'intégration et la compatibilité au niveau des interfaces de programmation avec les bibliothèques de la distribution dkpro.
	\end{itemize}
	\item faire des expériences de création et d'évaluation d'acceptions interlingues à petite échelle, avec deux ou trois langues pour lesquelles il existe des jeux de données étalons de référence, par exemple en réutilisant les étalons de référence produits par \citet{Matuschek2015}.
	\item développer les algorithmes de \ac{AAS} pour DBNary afin de produire des graphes de traduction sens à sens.
\end{itemize}

\subsubsection{À moyen terme}

Il est relativement limitant de construire et d'évaluer d'abord des alignements bilingues, puis dans une deuxième étape de créer les acceptions interlingues. Il sera intéressant de concevoir un algorithme général de \ac{AAS} permettant de créer ces alignements d'une manière qui intègre les contraintes axiomatiques pour la construction initiale d'acceptions interlingues. Cette approche peut se baser sur le développement de l'algorithme d'ajout par divergence, qui mènera à la production d'une mesure de divergence translingue intégrant directement ces contraintes. 

L'encodage des contraintes structurelles et combinatoires dans la définition de la mesure de divergence permettrait par ailleurs également de réaliser des plongements de mots multilingues projetés sur les acceptions interlingues, sur la base des travaux de \citep{Ammar2016}.

\subsubsection{À long terme}

Les objectifs à long terme principaux sont les suivants.

\begin{itemize}
\item Étendre les algorithmes à différents paradigmes de \ac{RLS}, notamment pour convertir les ressource existantes telles que \emph{BabelNet} dans une architecture d'alignement par pivot interlingue.
\item Démocratiser ce type d'architecture dans le domaine et de produire une grande ressource unique intégrant différents niveaux de granularité sémantique et différents domaines. 
\item Produire une plateforme collaborative généralisée pour la correction et la mise à jour de la ressource produite, avec un système de facettes d'annotations qui permettent de cibler les utilisateurs compétents pour telle ou telle tâche. Cette plateforme pourra par exemple se baser sur l'évolution des travaux sur l'index interlingue collaboratif de \citet{Bond2016}. 
\end{itemize}