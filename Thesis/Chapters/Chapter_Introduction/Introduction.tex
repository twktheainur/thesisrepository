% !TeX spellcheck = fr_FR
\setcounter{footnote}{0}

En \ac{TAL}, selon la définition de \citet{Witt2009}, le terme ressource langagière est un terme vaste regrou\-pant aussi bien des données langa\-gières (ressources statiques) que des outils perme\-ttant de les traiter (ressources dynamiques). Les formes les plus élémentaires de ressources langagières statiques sont les données langagières brutes (par exemple un corpus textuel, un signal de parole), qui n'ont subi aucune sélection particulière. 

Lorsque la sélection d'un sous-ensemble est faite à partir de données brutes, pour un but, un domaine, une contrainte ou une application particulière, les données du sous-ensemble sélectionné seront appelées <<ressource langagière primaire>>. Cette sélection peut être obtenue soit de manière experte, soit par un traitement effectué au moyen d'une ressource langagière dynamique. À ce niveau, les ressources langagières ne sont dotées d'aucune interprétation explicite.

\citet{Witt2009} présentent une autre distinction, orthogonale, des ressources langagières.  D'après eux, il faut faire une distinction entre les ressources langagières à base de texte et celles à base d'entrées. Les premières incluent non-seulement, les corpus mais aussi les outils qui permettent de traiter un texte dans son ensemble (analyseur syntaxique, outils de compréhension du langage etc.). Les deuxièmes comprennent les lexiques et tous les outils qui permettent de traiter les données par entrées (par exemple, un système de désambiguïsation lexicale, un analyseur morphologique).

Lorsque l'on souhaite ajouter ou expliciter des informations relatives aux données primaires, il faut les doter d'annot\-ations à différents niveaux en se conformant à la spécification d'une hiérarchie ou d'un empilement de types d'annotation (annot\-ation des méta-données, annotations linguistiques). Cette annotation peut être soit experte, soit le résultat d'un traitement automatique. 

\citet{Witt2009} proposent de caractériser la nature des étiquettes d'annotation (types) dans le cadre d'une méta-spécification visant à l'interopérabilité de la représentation des données (\autoref{fig:classif_ling_res}). 

\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.8\textwidth]{Figures/vector/Chapter1/classif_ling_res}
\end{center}
\caption[Classification des ressources langagières d'après (Witt et al. 2009).]{Classification des ressources langagières d'après \citet{Witt2009}.}
\label{fig:classif_ling_res}
\end{figure}

Cette thèse porte particulièrement sur les ressources lexico-sémantiques; par conséquent, nous restreindrons notre propos à ce type de ressources, même si certaines peuvent avoir une portée plus générale.

Lorsque l'on souhaite utiliser plusieurs de ces ressources ensemble, il est nécessaire de s'assurer qu'elles sont interopérables. 
Il existe trois niveaux d'inter\-opéra\-bilité (\autoref{fig:interop_steps}), qui se construisent successivement, chacun sur la base des précédents\footnote{C.à.d. que le niveau 1 est nécessaire pour arriver au niveau 2 et ainsi de suite.} \citep{Witt2009}:

\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.5\textwidth]{Figures/vector/Chapter1/interopSteps}
\end{center}
\caption{Les trois niveaux d'interopérabilité pour les ressources langagières.}
\label{fig:interop_steps}
\end{figure}

\begin{itemize}
	\item \emph{Interopérabilité représentationnelle (niveau 0)} Elle vise à s'assurer que les données des ressources sont représentées d'une manière compatible. Cela veut dire qu'il faut que les annotations associées aux données correspondent à des types d'annotation compatibles et issues d'une hiérarchie commune. Par exemple, dans le cas de deux dictionnaires, il faut s'assurer que les entrées et les sens correspondent aux mêmes éléments et que les données sont représentées et définies de la même manière~: nous ne pouvons pas directement comparer des sens de mots (point de vue lexicographique \footnote{Il s'agit de la vision classique en dictionnairique.}) à des concepts/synsets (point de vue cognitif, voir \autoref{def:synset}) sans une étape intermédiaire définissant le passage de l'un à l'autre\footnote{Ici, un sens est la lexicalisation d'un concept dans un contexte d'usage particulier.}. 
					\begin{definition}{Synset}{h!}
							L'idée du \emph{synset} est basée sur un modèle cognitif de la sémantique qui suppose que les <<concepts>> mentaux sont représentés par une association de mots qui peuvent apparaître ensemble dans certains contextes d'usage où leur sens est le même. Le \emph{synset} serait alors un \emph{synonym set} (ensemble de synonymes). Nombre de lexicographes considèrent qu'il n'existe un synonyme que si tous les contextes d'usage sont identiques, et de ce fait, les synsets ne sont pas des ensembles de synonymes, mais plutôt des ensembles de quasi-synonymes (\emph{near-synonyms} en anglais) \citep{Edmonds2002}.
							\label{def:synset}
					\end{definition}
	Ainsi, si nous prenons l'exemple de deux dictionnaires que nous voulons aligner, il faudra d'abord que nous assurer que la représentation soit la même, c'est-à-dire que les données soient représentées dans le même format et que les entrées soient définies de la même manière.
	
	\paragraph{} Dans l'état de l'art se développent des standards pour les données lexicales liées ouvertes (modèle \verb|lemon|/\verb|ontolex|) sur la base des technologies du web sémantique qui gagnent en popularité. En effet la plupart des ressources majeures ont maintenant une version en \verb|lemon|. Ces technologies rendent aisés l'alignement et l'accès aux ressources au niveau de la représentation, et sont une avancée significative dans la direction d'une représentation universelle pour les ressources lexicales (voir la \autoref{def:reslexsem} et le \autoref{chap:contexte}). 
	
	\begin{definition}{Ressource lexico-sémantique}{h!}
	\label{def:reslexsem}
		Une ressource lexico-sémantique est une ressource langagière à base d'entrées. Chaque entrée correspond à un mot du lexique et est associée à un ensemble de sens. Les sens dénotent les différents usages du mot ayant des significations différentes. L'ensemble des mots et des sens forment un graphe où différents mots et sens peuvent être reliés par des relations lexicales ou sémantiques (synonymie, antonymie, hypernymie, traduction, etc.). 
	\end{definition}

	\item \emph{Interopérabilité sémantique (niveau 1)} Lorsque les représentations des données sont compatibles, on peut vouloir aligner les éléments des différentes ressources et créer des liens entre eux (annotation d'éléments de la ressource par des éléments de la même ou d'autres ressources).
	
	 Chaque élément (phrase, mot, sens, entrée) porte des informations qui décrivent l'information sémantique portée par l'élément en question. Pour que deux éléments ou plus  soient jugés équivalents, il faut s'assurer que l'information sémantique portée par les éléments soit la même. 
	 
	 Si des éléments portent une information plus spécifique ou plus générale, une hiérarchie d'équivalences partielles est nécessaire pour assurer une interopérabilité au niveau sémantique, qui garantit qu'il n'y aura pas de perte d'information lorsque l'on utilise les ressources ensemble.
	 
	  Si l'on reprend l'exemple de deux dictionnaires, une fois que nous avons vérifié que leurs représentations sont compatibles, faut choisir comment sera représenté l'alignement entre les sens, de manière à ce que l'on puisse passer des sens d'un dictionnaire vers les sens de l'autre, idéalement sans perte des distinctions sémantiques. Le problème encore plus important lorsque l'on passe à une situation où les ressources sont toutes dans des langues différentes. 
	
	\paragraph{} Dans l'état de l'art, nous pouvons distinguer les approches par transfert qui alignent plusieurs ressources entre elles deux à deux, et les approches par pivot qui alignent tous les sens des ressources lexicales à une structure commune. 
	
	Cependant, les approches par pivot utilisent souvent les sens d'une langue ou d'une ressource en particulier comme pivot (pivot naturel).  Cela crée des pertes de contraste entre des langues présentant des distinctions sémantiques plus fines qui peuvent mener à des lexicalisations divergentes (par exemple \emph{river} en anglais qui peut à la fois se traduire par \emph{fleuve} et par \emph{rivière}). 
	
	La raison principale de ce choix est qu'il est difficile de construire un pivot indépendant de la langue: il y a peu d'experts parlant suffisamment de langues pour le construire manuellement et il n'y a pas de formalisation permettant de construire un tel pivot automatiquement (voir \autoref{chap:alignementinteroperabilite}).
	
	\item \emph{Interopérabilité dynamique (niveau 3)}~Les ressources langagières ne sont pas figées dans le temps. De nouvelles versions sortent et, dans le cas des ressources collaboratives, les changements se font de manière continue dans le temps. Lorsqu'une application utilise une ressource et souhaite, par exemple, bénéficier des ajouts de la nouvelle version, il faut s'assurer qu'une continuité est maintenue avec la nouvelle version. De plus, si l'on apporte des modifications à une ressource par un procédé quelconque, il faut pouvoir réconcilier les nouvelles informations avec de futures modifications qui seront apportées à la ressource. 
	\begin{figure}[h!]
		\begin{center}
		\includegraphics[width=0.8\textwidth]{Figures/vector/Chapter1/dynamicity_diagram}
		\end{center}
		\caption{Une illustration du besoin d'interopérabilité dynamique sur l'exemple de \babelnet.}
		\label{fig:iterop_dyn}
		\end{figure}	
		
	 La \autoref{fig:iterop_dyn} illustre ce problème au travers d'une situation hypothétique qui prend l'exemple de BabelNet, qui est construit en partie sur Wikipédia, une ressource collaborative.
	 \begin{enumerate}
	 	\item Supposons que BabelNet 1.1 a été généré à une date \(N\) (étape 1),
	 	\item et que, par la suite, il a été nécessaire d'apporter des corrections manuelles directement dans BabelNet 1.1 pour obtenir BabelNet 1.1.1 (é\-tape 2) \citep{Navigli2013},
	 	\item puis qu'à une date \(N+1\) Wikipédia a changé (ajout/suppression de pages, modifications)  du fait de nouvelles contributions (étape 3),
	 	\item et qu'enfin la version 1.2 de BabelNet a été générée automatiquement en partie à partir de cette version modifiée de Wikipédia (étape 4). Alors, les modifications manuelles faites dans BabelNet 1.1.1 doivent également être manuellement transférées dans BabelNet 1.2. 	 	
	 \end{enumerate} 

	\paragraph{} À l'heure actuelle, dans l'état de l'art, il n'existe pas de travaux portant sur une interopérabilité dynamique systématique et garantie d'un alignement de ressources lexicales, que ce soit avec une architecture pivot ou une architecture par transfert. 
	Pour certaines \ac{RLS}, des mesures \emph{ad hoc} peuvent être mises en place à chaque changement de version (voir  \wordnet~\citep{fellabaum1998} ou \babelnet~\citep{Navigli2013}). 
\end{itemize}


\paragraph{Synthèse des verrous et contraintes.} [\textbf{Contrainte 1}] Ce qui ressort de cette analyse est qu'à l'heure actuelle \emph{il existe des standards et des formats de données permettant d'assurer une bonne interopérabilité représentationnelle des ressources lexico-sémantiques et qui sont utilisées de manière de plus en plus répandue}. 

\paragraph{} [\textbf{Verrou 1}] Cependant, pour certains types de ressources langagières, et plus particulièrement les \ac{RLS}, des problèmes d'interopérabilité se manifestent au niveau sémantique lorsque l'on veut aligner plusieurs ressources entre elles, en particulier dans un contexte multilingue.

 Ainsi, une architecture par transfert n'est pas viables pour un nombre important de paires de langues et pose des problèmes d'accès. Ce problème n'est pas présent avec les architectures par pivot, cependant les représentations pivot utilisées dans l'état de l'art sont biaisée car le pivot est une langue naturelle. 
 
 Cela pose des problèmes de perte de contraste dans les cas de lexicalisations divergentes. Par ailleurs, des représentations  pivot artificielles ne dépendant d'aucune des langues existent, \emph{cependant elles sont difficiles à construire manuellement}.

\paragraph{} [\textbf{Verrou 2}] Il \emph{n'existe pas non plus de vraie formalisation de ces représentations pour permettre la conception d'algorithmes de création automatique}. 

\paragraph{} [\textbf{Verrou 3}] Enfin, \emph{la dimension de la dynamicité n'est à l'heure actuelle pas du tout considérée, alors que de plus en plus de \ac{RLS} sont générées à partir de ressources collaboratives qui évoluent régulièrement} . 

\paragraph{Objectifs de la thèse.}

Au vu des verrous et de la contrainte, l'objectif de cette thèse est de proposer des solutions algorithmiques et techniques pour assurer l'interopérabilité au niveau sémantique de \ac{RLS} multilingues par acceptions interlingues (pivot artificiel), et de proposer des applications et des moyens d'évaluation pratiques. 

Les objectifs finals étant très ambitieux, en particulier vis-à-vis de l'application pratique et de l'évaluation, nous délimitons donc le sujet comme suit.
\begin{enumerate}
\item Même si les algorithmes et méthodes présentés dans cette thèse sont génériques, le côté applicatif se focalisera sur une ressource lexico-sémantique, DBNary \citep{serasset:dbnary-swj}. DBNary  est représenté en données lexicales liées ouvertes par l'intermédiaire du modèle \verb|lemon|/\verb|ontolex| (voir la section \ref{sec:interopestatbaseentrees}) \citep{McCrae2012}.
\item Nous commencerons en nous plaçant dans un contexte formel afin de bien définir les propriétés attendues des acceptions interlingues. En effet, l'introduction initiale des acceptions interlingues les qualifie et les définit avec des termes formels, sans toutefois de spécification ou d'axiomatisation concrètes \citep{serasset1994}.  
\item Même si nous ne nous intéressons pas directement à l'interopérabilité dynamique, nous veillerons à définir des opération de mise à jour compatibles avec une prise en compte ultérieure de cette dernière. 
\end{enumerate} 

\section*{Plan du manuscrit}

Afin de mener à bien les objectifs, nous étudions dans une première partie l'état de l'art relatif aux standards et aux formats pour l'interopérabilité, ainsi qu'aux algorithmes relatifs à l'alignement automatique de \ac{RLS} multilingues. 

Nous passons ensuite aux contributions de ce travail de thèse par rapport aux objectifs sus-cités. Nous présentons dans un premier temps les aspects formels et les algorithmes portant sur les acceptions interlingues. Dans un deuxième temps, nous présenterons les contraintes liées à une application pratique et à une évaluation des ressources alignées avec un pivot par acceptions interlingues.  

Nous résumons ci-dessous, pour chaque chapitre son contenu de manière plus détaillée.

\subsection*{Partie 1}
\paragraph{Chapitre \ref{chap:contexte}.}
Ce chapitre vise à présenter les standards existants pour l'interopérabilité des \ac{RLS}, puis à présenter les différents types de ressources en les classant en fonction du degré d'interopérabilité sur les trois niveaux (représentationnel, sémantique, dynamique) possibles selon de l'état de l'art. Enfin, pour chaque type de ressource, le chapitre présente l'étude de cas des ressources majeures du type. La ressource utilisée dans la partie pratique de ce travail, DBNary y sera décrite de manière détaillée.

\paragraph{Chapitre \ref{chap:alignementinteroperabilite}.} 

Ce chapitre présente d'abord l'état de l'art des algorithmes d'\ac{AAS}, et en particulier les tâches similaires dans d'autre domaines, les algorithmes d'alignement en eux-mêmes, puis la question particulière de l'alignement de ressources multilingues.
Il présente ensuite de manière détaillée les différentes architectures d'interopérabilité sémantique évoquées dans cette introduction, ainsi que les problèmes qui se posent avec l'approche habituelle de l'état de l'art. 
Il aborde enfin la question des acceptions interlingues et des différents travaux portant sur elles depuis leur introduction. Il passe notamment en revue des tentatives antérieures pour la construction automatique d'acceptions interlingues. 

\subsection*{Partie 2}

\paragraph{Chapitre \ref{chap:contraintesobjectifs}.} Ce chapitre regroupe l'ensemble des contributions théoriques con\-cer\-nant les acceptions interlingues. Il propose d'abord une formalisation des \ac{RLS} puis, à partir des travaux antérieurs sur les acceptions interlingues, définit les axiomes et les  propriétés les régissant et énonce des théorèmes portant sur les contrainte combinatoires de leur création. Le chapitre propose ensuite un algorithme de construction naïf  reposant sur les propriétés combinatoires du graphe de traduction et sur la nature récursive des hiérarchies d'acceptions interlingues (tout en prenant en compte les axiomes et théorèmes). L'algorithme étant d'une complexité dans le pire des cas factorielle (\(O(n^n\cdot n!)\)), un algorithme plus performant est proposé, exploitant l'ordre de dégénérescence du graphe; sa complexité dans le pire des cas est bornée par l'identification des cliques (exponentielle, \(O({n}^2\cdot 3^{n})\)). 
Enfin sont proposées des techniques de mise à jour (ajout, suppression, correction) d'alignements par acceptions interlingues, dans le but de ne pas avoir besoin de trouver toutes les paires d'alignements. 

\paragraph{Chapitre \ref{chap:alignemntacception}.} Ce chapitre aborde les aspect plus pratiques préalables à la création d'acceptions interlingues dans le cas de DBNary. Il présente d'abord une expérience visant à ramener les relations de traduction au niveau de sens à la source en exploitant les données de Wiktionary. Ensuite, il présente les mesures de similarité translingue et les algorithmes d'alignement pertinents pour produire tous les alignements deux à deux des 21 éditions de langues de DBNary. Suit la présentation des techniques d'évaluation qui peuvent être utilisées afin de déterminer la qualité des pivots produits, et la proposition d'un protocole expérimental pour une évaluation in vivo. Nous terminons en présentant LexSemA, la plateforme logicielle développée pour servir de base à la création d'acceptions interlingues, et qui regroupe tous les outils nécessaires.

\section*{Contributions \& résultats principaux}

Nous pouvons  identifier les contributions et résultats principaux suivants.

\begin{enumerate}
	\item~[\textbf{Chapitre 3}]~Formalisation axiomatique des RLS et des pivots par acceptions interlingues. 
	\item~[\textbf{Chapitre 3}]~Définition d'un algorithme de construction d'acceptions interlingues optimal d'un point de vue combinatoire et axiomatique.
	\item~[\textbf{Chapitre 3}]~Définition d'algorithmes d'insertion, de suppression et de mise à jour pour les ressources alignées par acceptions interlingues. 
	\item~[\textbf{Chapitre 4}]~Rattachement des relations de traduction de DBNary aux sens source avec une qualité comparable à celle de l'état de l'art. 
	\item~[\textbf{Chapitre 4}]~Définition d'une méthode globale pour l'alignement bilingue au niveau des sens pour l'ensemble des éditions de Wiktionary. 
	\item~[\textbf{Chapitre 4}]~Définition d'un protocole expérimental visant à l'évaluation \emph{in vivo} des ressources à base d'acceptions interlingues. 
\end{enumerate}

%\printbibliography[title=Références]