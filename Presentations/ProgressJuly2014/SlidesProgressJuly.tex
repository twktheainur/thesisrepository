\documentclass[10pt,a4paper,mathserif]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{makeidx}
\usepackage{graphicx}
\usepackage{ragged2e}
\author{Andon Tchechmedjiev}
\title{Acceptional Associative Subset Lattices: Formal Properties and Topology -- Towards the induction of interlingual acceptions }
\usetheme{Warsaw}
\usecolortheme{beaver}

\newtheorem{axiom}{Axiom}
\newtheorem{corrolary}{Corrolary}
\newtheorem{lem}{Lemma}
\newtheorem{prop}{Property}

\makeatletter
\addtobeamertemplate{theorem begin}{%
  \expandafter\NR@gettitle\expandafter{\inserttheoremaddition}%
}{}
\makeatother

\begin{document}

\input{../../Thesis/Config/notations.tex}
\begin{frame}
\maketitle
\end{frame}

\section{Problem Statement}
\begin{frame}
\frametitle{Problem Statement -- What we have}
\includegraphics[width=\textwidth]{../../img/dbnarystrc3}
\end{frame}

\begin{frame}
\frametitle{Problem Statement -- What we want}
\includegraphics[width=\textwidth]{../../img/dbnarystrc9}
\end{frame}

\begin{frame}
\frametitle{Problem Statement -- What we want -- No language pivot!}
\includegraphics[width=\textwidth]{../../img/acception}
\end{frame}

\section{Related Work}
\begin{frame}
	\frametitle{Related Work}
	\begin{itemize}	
		\item Traslanslations and sense graphs:
		\begin{itemize}
		 \item  Acception graph, inference of new translation relations 
		 \item Detects ambiguity -- Completely connected shared closure (Mausam et al. 2009)
		\end{itemize}
		\item Lexical networks: 
		\begin{itemize}
			\item PanLex  (Kamholz et al. 2014)
			\item YaMTG (Hanoka \& Sagot, 2014) << DBnary...
		\end{itemize}
		\item LR construction:
		\begin{itemize}
			\item Babelnet (Babelify) (Navigli, Ponzetto, Pihevar), 
			\item Uby (Eckle-Kohler, Gurevych), 
			\item Wikipedia + Wiktionary (Meyer, Miller, Gurevych)(Navigli, Pilhevar)
		\end{itemize}
	\end{itemize}
\end{frame}


\section{Lessons Learned}
\begin{frame}
\frametitle{Lessons Learned}
Some observations:
\begin{itemize}
	\item Finding possible sources (acceptions) for translation relation is relatively easy, everyone is doing it +  good performance\\
	\hspace{1em} 	{\footnotesize (Tchechmedjiev et al., Pilhevar et al. 2014; Gurevych et al. 2013)}
	\item We have (almost) no solutions to easily find the right potential targets, combinatorially heavy
	\item How do we evaluate it?
\end{itemize}
\vspace{2em}
Any solutions?
\begin{itemize}
	\item Let's not forget our objective: Create interlingual acceptions
	\item Can we see target elicitations as a \textbf{by-product of acception induction?}
	\item Identified sources allow us to prune the search space greatly
\end{itemize}
\end{frame}

\section{Scientific Challenges}
\begin{frame}
\frametitle{Scientific Challenges}
	\begin{itemize}
		\item \textbf{(C1)} Obtaining good cross-lingual semantic proximity or divergence measures
		\item \textbf{(C2)} How to evaluate? \\ \hspace{1em} {\footnotesize (WordNet ILI + Corrections? -- Data from Dikonov?)}
		\item \textbf{(C3)} Combinatorial dimensionality
		\item \textbf{(C4)} How to combine noisy measures in a complimentary fashion?
		\item \textbf{(C5)} What is the right hierarchical clustering model?
	\end{itemize}
\end{frame}


\section{Requirements}
\begin{frame}
\frametitle{Requirement}
	\begin{itemize}
		\item (R1) We need a better formal understanding of the problem 
		\item (R2) We need good cross-lingual measure
		\item (R3) That we can somehow combine and extend over sets
		\item (R4) We need an efficient way of computing that
		\item (R5) We need a some form of hierarchical clustering model for interlingual acceptions
	\end{itemize}
\end{frame}

\section{Formalization}
\begin{frame}
	\frametitle{Definitions \& Notations}
	\begin{definition}[\textbf{Lexie} --- \(\lexie\)]
	\justifying	The \emph{lexie} is used as the lexical unit of language and can either be a \emph{lexeme} or a phraseme.
	\end{definition}
	\begin{definition}[\textbf{Acception} --- \(\sense\)]
		\justifying From the Latin \emph{acceptio}, equivalent to the ancient Greek word \(\sigma\zeta\mu\alpha\sigma\iota\alpha\) (simasía) that has given the English word ``\emph{sense}''. An acception is the \emph{signified} of a lexie. In the usual vocabulary of word sense disambiguation, acceptions can be considered as word senses as defined in a dictionary. The signified of an acception is often described by an expression called definition.
	\end{definition}
\end{frame}

\begin{frame}
	\frametitle{Definitions \& Notations}
	\begin{definition}[\textbf{Lexicon}---\(\lexicon\)]
		 \justifying The lexicon of a language is composed by a set of \emph{lexis}/\emph{lexies}, the lexical units of the language.
	\end{definition}
	
\begin{definition}[\textbf{Lexeme}---\(\lexeme\)] \justifying A \emph{lexeme} is a \emph{lexis} considered in one particular acception (sense) accompanied by all the information specifying its semantic behaviour when it is used in that particular acception.
\end{definition}

\begin{definition}[\textbf{Vocable} --- \(\vocable\))] 
A vocable is the set of all lexies that satisfy the following conditions:
\begin{itemize}
	\item their signifiers are identical;
	\item There is a pairwise semantic links between pairs of composing lexies.
\end{itemize}
\end{definition}
 \end{frame}

\begin{frame}
	\frametitle{Formal Properties and Axioms}
	\begin{axiom}[ \textbf{Lexies ---\(\lexie\) \& Lexicon ---\(\lexicon\)}]For any given language are associated on or more lexica \(\lexicon_i\):  The element \(\lexie \in \lexicon_i\) are the lexies for that language resource.
	\end{axiom}
	\begin{axiom}[\textbf{{Acceptions}}---\(\sense\)] For each lexie \(\lexie\) of a lexicon \(\lexicon\), a function \(\senses(\lexie)\) gives the list of possible acceptions/senses \(\sense_i\) for that lexie~---~\(\senses: \lexicon\mapsto \senses_\lexie\). 
	\end{axiom}
	\begin{axiom} [\textbf{Signifiers}---\(signf\)]\justifying A particular acception \(\sense\) is accompanied by information that defines the signified of that acception, we write \(signf(\sense, type)\) the function that returns a particular type of defining information. For the textual definition associated we use the shorthand \(def(\sigma)=signf(\sense,def)\).
	\end{axiom}
\end{frame}
\begin{frame}
	\frametitle{Formal Properties and Axioms}
\begin{axiom}[Set of lexical resources]
\[\mathfrak{LR} = \{ \lexicon_1, \lexicon_2, \ldots , \lexicon_n\}\]
\end{axiom}
\begin{axiom}[Set of all lexies]
\[\Omega_\mathfrak{LR} = \mathop{\cup}_{\lexicon_i \in \mathfrak{LR}}\lbrace\lexicon_i\rbrace\]
\end{axiom}
\begin{axiom}[Vocables]
\[\vocable_x \in \vocables = \mathop{\cup}_{\lexie_i \in \lexicon}\{\lexie_i\}\]
\end{axiom}
\begin{axiom}[Set of all acceptions]
\[\Omega_\senses = \mathop{\cup}_{\lexicon_i \in \mathfrak{LR}}\mathop{\cup}_{\lexie_j \in \lexicon_i} \senses(\lexie_j) =  \mathop{\cup}_{\lexie_i \in \Omega_\mathfrak{LR}}\senses(\lexie_i)\]
\end{axiom}
\end{frame}
\begin{frame}
	\frametitle{Formal Properties and Axioms}
\begin{axiom}[Vocables]
\[\vocable_x \in \vocables = \mathop{\cup}_{\lexie_i \in \lexicon}\{\lexie_i\}\]
\end{axiom}
\begin{axiom}[Set of all acceptions]
\[\Omega_\senses = \mathop{\cup}_{\lexicon_i \in \mathfrak{LR}}\mathop{\cup}_{\lexie_j \in \lexicon_i} \senses(\lexie_j) =  \mathop{\cup}_{\lexie_i \in \Omega_\mathfrak{LR}}\senses(\lexie_i)\]
\end{axiom}
\end{frame}
\begin{frame}
	\frametitle{Formal Properties and Axioms}
\begin{axiom}[Lexie level translation relation]
\begin{align*}
\vspace{-1em}
\transl~:~  \lexicon_i  & \longrightarrow  \lexicon_j \\
 \lexie_x & \longmapsto    \lexie_y
\end{align*}
\end{axiom}	
\begin{axiom}[Acception transation relation]
\begin{align*}
\transl~:~  \Omega_\senses  & \longrightarrow  \Omega_\senses \\
 \sense_x & \longmapsto    \sense_y
\end{align*}
\[\Theta =  \Omega_\senses \times \Omega_\sense, \exists \sense_i \in \Omega_\senses, \exists \sense_j \in  \Omega_\senses : \sense_i\transl\sense_j\]
\[\transl_{i,j} \in \Theta : \transl(\sense_i)=\sense_j \mbox{ or }  \sense_i\transl\sense_j \]
\end{axiom}
\end{frame}

\begin{frame}
	\frametitle{Formal Properties and Axioms}
\begin{axiom} 
\[\exists! \sense_i\theta\sense_j, \sense_i\in\senses(\lexie_x), \sense_j \in \senses(\lexie_y) \Leftrightarrow \lexie_x\transl\lexie_y\]
There is a translation link between the two lexies \(\lexie_x\) and \(\lexie_y\) if and only if there exists at least one translation link between two acceptions \(\sense_i\in\lexie_x\) and \(\sense_j\in\lexie_y\), 
\end{axiom}
\begin{corrolary}
	The previous axiom implies that there exists at least one translation link between \(\sense_i\) and \(\sense_j\), however, we do not know exactly how many there are. In fact the set of all translations between \(\senses(\lexie_x)\) and \(\senses(\lexie_y)\) is a subset of \(\senses(\lexie_x)\times\senses(\lexie_y)\).\\
	\textbf{Previous work:} Select one or more \(\sense_i \in \senses(\lexie_x)\), i.e. find the most likely correct sources.
\end{corrolary}
\hspace{1em} 
\end{frame} 

\begin{frame}
	\frametitle{Interlingual Acceptions}
In the more general setting of building interlingual acceptions for a given lexie \(\lexie_k\in\Omega_\lexicon\), we need to consider the set of lexies \(\lexie_i\in\Omega_\lexicon\) such that  \(\forall \lexie_i, \lexie_k\transl\lexie_i\) holds. Then we need to work on all the senses of \(\lbrace\lbrace{\lexie_k}\rbrace\cap\lbrace{\lexie_i \in \Omega_\lexicon}\rbrace \mid \lexie_k\transl\lexie_i\rbrace\). To this end, we define: 
\begin{axiom}[Lexie translation closure acception set]
\[\lexie_x \in \Omega_\lexicon, \Omega_\senses[\lexie_i] \subset \Omega_\senses = \mathop{\cup}_{\lexie_i \in \Omega_\lexicon : \lexie_x\transl\lexie_i} (\senses(\lexie_i))\]
\end{axiom}
Similarly, \(\Theta[\lexie_i]\) is the restriction of \(\Theta\) to \(\Omega_\senses[\lexie_i]\times \Omega_\senses[\lexie_i]\)
\begin{axiom}[Interlingual Acception]
	Given the set \(\Theta[\lexie_i]\), an interlingual acception is a subset \(I \subseteq \Theta[\lexie_i]\). An interlingual acception referring to a refinement of meaning of another interlingual acception is a subset of \(I\).
\end{axiom}
\end{frame}
\begin{frame}
	\frametitle{Interlingual Acceptions}
\only{
\begin{axiom}[Local acception association lattice]
The set of all possible interlingual acceptions for a lexie \(\lexie_x\) is the set of all subsets of \(\Theta[\lexie_i]\), in other words the power set \(2^{\Theta[\lexie_i]}\). \((2^{\Theta[\lexie_i]},\subseteq)\) is thus a partial order over \(2^{\Theta[\lexie_i]}\) and can be represented by a lattice.
\end{axiom}}<1>
\only{\centering \includegraphics[width=0.8\textwidth]{../../img/acception_formal}}<2>
\end{frame}

\section{Combinatorial Consideration}
\begin{frame}
	\frametitle{Combinatorial properties}
	\begin{itemize}
		\item Power set -- all non-overlapping subsets \\
		{\footnotesize Not partitions! We don't care about the order. We don't need/want all elements}
		\item Size: \(2^n\)\\
		\hspace{1em} { \footnotesize E.g. 12 \(\sigma\) in 3 graphs \(\rightarrow\) up to \(3\times 4\times 4=48\) pairs}\\
		\hspace{1em} {\footnotesize \(2^{48} \simeq 2.81\times 10^{14}\)  sets}
		\item Incomplete and noisy data \\
		\hspace{1em} \(\Rightarrow\) {\footnotesize Noisy measures \& search space (c.f. Festschrift chapter)}
		\begin{itemize}
			\item Measures between acceptions: \\ \hspace{1em}{\footnotesize incomplete features, imprecise estimators}
			\item Sets of acception pairs: Noise accumulation, Imperfect combination
		\end{itemize}
	\end{itemize}
\end{frame}
\section{Semantic proximity \& Divergence measures}
\begin{frame}
	\frametitle{Measures of proximity and divergence -- Local}
		Acception association \(a\theta b\) level -- Feature-based CL similarities
				\begin{itemize}
					\item Textual overlap/divergence (+ extensions) \\
					\hspace{1em} \(\Rightarrow\) Requires comparable definitions/glosses, examples\\
					\hspace{1em} \(\Rightarrow\) Lexical transfer needed:
					\begin{itemize}  
					\item Direct (Naïve approach)
					\item Machine Translation \\ 
					\hspace{1em} {\footnotesize(Banea et al.)(Miller et al.) (Navigli \& Ponzetto)\(\ldots\)} + [us, implemented]
					\item Transitive closure transfer (bias-based) [us, implemented]
					\item Distributional semantics based \\
						\hspace{1em} {\footnotesize CL WSD work, e.g. Apidianaki,2014 @ TALN, Semeval Tasks}
					\end{itemize}
					\item Graph vertex/edge overlap \\ \hspace{1em} {\footnotesize J\&C, W\&P, Resnik, \(ldots\)), GED (Gaume et al. 2014)}
				\end{itemize} 
\end{frame}

\begin{frame}
	\frametitle{Measures of proximity and divergence -- Local}
		Acception association \(a\theta b\) level -- CL relatedness measures \\ 
\begin{itemize}
\item Statistical /Vectorial / Distributional semantics
				\begin{itemize}
					\item On features: requires enough features (e.g. small corpus from definitions + extention)
					\item From corpora
					\item Many measures 
					\begin{itemize}
						\item Euclidean measures (if distance constraints hold!)
						\item Relaxation of triangle inequality \& symmetry -- Bregman divergences \\ 
						\hspace{1em} {\footnotesize Generalizes properties needed for clustering (Nook \& Nielsen)} 
					\end{itemize}
				\end{itemize}
\item Topological -- Similarity in terms of shape, e.g. Based on random walks, cf (Gaume et al. 2014)
\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Measures of proximity and divergence -- Global}
	\begin{itemize}
	\item We want to associate to each subset \(A \subseteq \Omega_\senses\) a fitness whether positive or negative
	\begin{align*}
	F~:~  2^{\Omega_\senses}  & \longrightarrow  \mathfrak{R} \\
	 A & \longmapsto    x
	\end{align*}
	\item Can exploit interesting combinatorial properties?
	\begin{itemize}
		\item Submodularity!
		\item \(F~:~2^{V} \longrightarrow \mathfrak{R}\) is submodular iif\\
		\hspace{1em} \(F(A)+F(B) \geq F(A\cup B)+F(A \cap B)\)
		\item Optimality guarantees on minimisation \& Maximisation
		\item Continuous convex extension allows to build a Submodular Lovàsz-Bregman divergence compatible with continuous bregman divergences (Bilmes \& Students) \(\Rightarrow\) convenient combination of local measures. 
	\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{From local to global}
	\begin{itemize}
		\item We can define local measures using submodularity to optimise the features used under constraints and assign weights
		\item We can combine several measures as a sensor/camera placement model and find the weights and the measures to use that maximise the contribution of each individual measure \(F(A)= \lambda_1 F_1(A)+\ldots+\lambda_n F_n(A)\). If we define \(w=\lbrace\lambda_1,\ldots, \lambda_n\rbrace\) and \(F'=\lbrace F_1(A), \ldots F_n(A)\rbrace\), then \(F(A)=w(F')=\sum_{w_i\in w, F_i \in F'}(w_i F_i)\) is submodular. The Lovász extention gives us a continuous extention that can be optimised as a continuous function to find the best combination.
	\end{itemize}
\end{frame}
\begin{frame}
	\frametitle{From local to global -- Example}

 Let \(Obj(\transl_{i,j})=\sense_j\) and \(Subj(\theta_{i,j})=\sigma_i\). \\
 Let \(SO=signf(Obj(\theta_{i,j}))\) and \(SS=signf(Subj(\theta_{i,j}))\)\\
			\begin{align*}
			FTv~:~  \Theta  & \longrightarrow  \mathfrak{R}^{+} \\
			 \transl_{i,j} & \longmapsto  \sum_{(a,b)\in SO \times SS} sim(a,b) \\&+ \alpha \sum_{i_{SO \backslash SS} \in 1_{SO\backslash SS}} i_{SO \backslash SS} \\ &+ \beta \sum_{i_{SS\backslash SO} \in 1_{SS\backslash SO}} i_{SS\backslash SO} 
			\end{align*}
\end{frame}
\section{Towards clustering interlingual acceptions}
\begin{frame}
	\frametitle{Some ideas}
	\begin{enumerate}
	\item Starting from the top, fully split in two at each level
	\item Then on the existing hierarchical clustering, apply local fusion decisions
	\item Then again splitting locally
	\item Repeat until it converges
	\end{enumerate}
\end{frame}

\end{document}